/*
    SPDX-FileCopyrightText: 2021 Volker Krause <vkrause@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef CONFIG_ITINERARY_H
#define CONFIG_ITINERARY_H

#cmakedefine01 HAVE_KCRASH
#cmakedefine01 HAVE_KHEALTHCERTIFICATE
#cmakedefine01 HAVE_KUNITCONVERSION
#cmakedefine01 HAVE_MATRIX

#endif
